set(DOCUMENTATION "Phenology analysis and fitting.")

# define the dependencies of the include module and the tests
otb_module(OTBPhenology
  DEPENDS
  OTBITK
  OTBCommon
  OTBApplicationEngine
  OTBBoost
  OTBTemporalGapFilling
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    "${DOCUMENTATION}"
)
