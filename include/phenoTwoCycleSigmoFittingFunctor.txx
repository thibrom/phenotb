/*=========================================================================

  Program:   phenotb
  Language:  C++

  Copyright (c) Jordi Inglada. All rights reserved.

  See phenotb-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef _PHENO2CFF_TXX_
#define _PHENO2CFF_TXX_
#include "phenoTwoCycleSigmoFittingFunctor.h"

namespace pheno
{
template <typename PixelType>
TwoCycleSigmoFittingFunctor<PixelType>::TwoCycleSigmoFittingFunctor() : 
  return_fit{true}, return_metrics{false}, 
  fit_only_invalid{true}, use_mask{true} {};

template <typename PixelType>
void TwoCycleSigmoFittingFunctor<PixelType>::SetDates(const std::vector<tm>& d) {
  dates = d;
  dv = VectorType{static_cast<unsigned int>(dates.size())};
  for(size_t i=0; i<dates.size(); i++)
    {
    dv[i] = GapFilling::doy(dates[i]);
    }

}

template <typename PixelType>
void TwoCycleSigmoFittingFunctor<PixelType>::SetUseMask(bool um) {
  use_mask = um;
}

template <typename PixelType>
void TwoCycleSigmoFittingFunctor<PixelType>::SetReturnFit(bool rf) {
  return_fit = rf;
}

template <typename PixelType>
void TwoCycleSigmoFittingFunctor<PixelType>::SetReturnMetrics(bool rm) {
  return_metrics = rm;
}

template <typename PixelType>
void TwoCycleSigmoFittingFunctor<PixelType>::SetFitOnlyInvalid(bool foi) {
  fit_only_invalid = foi;
}

// dates is already given in doy
// valid pixel has a mask==0
template <typename PixelType>
PixelType TwoCycleSigmoFittingFunctor<PixelType>::operator()(PixelType pix, 
                                                             PixelType mask)
{
  auto nbDates = pix.GetSize();

  auto tmp_mask = mask;
  if(!use_mask)
    tmp_mask.Fill(0);

  if(dates.size()!=nbDates) throw DifferentSizes{};

  PixelType tmpix{nbDates};
  tmpix.Fill(typename PixelType::ValueType{0});

  // If the mask says all dates are valid, keep the original value
  if(tmp_mask == tmpix && fit_only_invalid && use_mask) return pix;
    
  VectorType vec(nbDates);
  VectorType mv(nbDates);

  for(std::size_t i=0; i<nbDates; ++i)
    {
    vec[i] = pix[i];
    mv[i] = tmp_mask[i];
    }
    
  // A date is valid if it is not NaN and the mask value == 0.
  auto pred = [&](int e) { return !(std::isnan(vec[e])) &&
      (mv[e]==(typename PixelType::ValueType{0})); };
  auto f_profiles = filter_profile(vec, dates, pred);

  decltype(vec) profile=f_profiles.first;
  decltype(vec) t=f_profiles.second;

  // If there are not enough valid dates, keep the original value
  if(profile.size() < m_MinNbDates) return pix;

  auto approx = normalized_sigmoid::TwoCycleApproximation(profile, t);
  auto x_1 = std::get<0>(std::get<1>(approx));
  auto mm1 = std::get<1>(std::get<1>(approx));
  auto x_2 = std::get<0>(std::get<2>(approx));
  auto mm2 = std::get<1>(std::get<2>(approx));

  if(return_fit)
    {
    return ComputeFit(x_1, mm1, x_2, mm2, nbDates, pix, mv);
    }
  if(return_metrics)
    {
    return ComputeMetrics(approx);
    }
  return ExtractParameters(x_1, mm1, x_2, mm2);
}

template <typename PixelType>
PixelType TwoCycleSigmoFittingFunctor<PixelType>::ExtractParameters(const VectorType& x_1, 
                                                                    const MinMaxType& mm1, 
                                                                    const VectorType& x_2, 
                                                                    const MinMaxType& mm2) const
{
  PixelType result(12);
  result[0] = (mm1.second-mm1.first);
  result[1] = mm1.first;
  for(auto i=0; i<4; ++i)
    result[i+2] = x_1[i];

  result[6] = (mm2.second-mm2.first);
  result[7] = mm2.first;
  for(auto i=0; i<4; ++i)
    result[i+8] = x_2[i];

  return result;
}

template <typename PixelType>
PixelType TwoCycleSigmoFittingFunctor<PixelType>::ComputeFit(const VectorType& x_1, 
                                                             const MinMaxType& mm1, 
                                                             const VectorType& x_2, 
                                                             const MinMaxType& mm2, 
                                                             std::size_t nbDates, 
                                                             const PixelType& pix, 
                                                             const VectorType& mv) const
{
  // Compute the approximation
  auto tmpres = normalized_sigmoid::F(dv, x_1)*(mm1.second-mm1.first)+mm1.first
    + normalized_sigmoid::F(dv, x_2)*(mm2.second-mm2.first)+mm2.first;
  // The result uses either the original or the approximated value
  // depending on the mask value
  PixelType result(nbDates);
  for(size_t i=0; i<nbDates; i++)
    if(fit_only_invalid)
      result[i] = ((mv[i]==(typename PixelType::ValueType{0}))?pix[i]:tmpres[i]);
    else
      result[i] = tmpres[i];

  return result;
}

template <typename PixelType>
PixelType TwoCycleSigmoFittingFunctor<PixelType>::ComputeMetrics(const TwoCycleApproximationResultType& approx) const
{
  PixelType result(12);
  auto princ_cycle = std::get<1>(approx);
  auto x_princ = std::get<0>(princ_cycle);
  auto m_princ = pheno::normalized_sigmoid::pheno_metrics<double>(x_princ);
  result[0] = std::get<0>(m_princ);
  result[1] = std::get<1>(m_princ);
  result[2] = std::get<2>(m_princ);
  result[3] = std::get<3>(m_princ);
  result[4] = std::get<4>(m_princ);
  result[5] = std::get<5>(m_princ);

  auto sec_cycle = std::get<1>(approx);
  auto x_sec = std::get<0>(sec_cycle);
  auto m_sec = pheno::normalized_sigmoid::pheno_metrics<double>(x_sec);
  result[6+0] = std::get<0>(m_sec);
  result[6+1] = std::get<1>(m_sec);
  result[6+2] = std::get<2>(m_sec);
  result[6+3] = std::get<3>(m_sec);
  result[6+4] = std::get<4>(m_sec);
  result[6+5] = std::get<5>(m_sec);

  return result;
}

template <typename PixelType>
bool TwoCycleSigmoFittingFunctor<PixelType>::operator!=(const TwoCycleSigmoFittingFunctor a)
{
  return (this->dates != a.dates) || (this->dv != a.dv) ;
}

template <typename PixelType>
bool TwoCycleSigmoFittingFunctor<PixelType>::operator==(const TwoCycleSigmoFittingFunctor a)
{
  return !(*this == a);
}

}//namespace pheno
#endif
