#Contact: Jordi Inglada <jordi.inglada@cesbio.eu>

otb_fetch_module(OTBPhenology
  "Tools for performing phenology analysis in image time series."
  GIT_REPOSITORY http://tully.ups-tlse.fr/jordi/phenotb.git
  GIT_TAG 7fef25ada53971b28810f032ce94077228445177
  )
