# =========================================================================
# Program:   phenotb
# Language:  C++
#
# Copyright (c) CESBIO. All rights reserved.
#
# See LICENSE for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
set(${otb-module}_SRC
  phenoFunctions.cxx 
  miscUtils.cxx
  )

add_library(${otb-module} ${${otb-module}_SRC})
target_link_libraries(${otb-module} ${OTBCommon_LIBRARIES} ${OTBITK_LIBRARIES} ${OTBBoost_LIBRARIES} ${GSL_LIBRARIES})
otb_module_target(${otb-module})

#target_link_libraries(OTBPheno ITKOptimizers boost_regex)
